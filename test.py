import boto3
import json
import requests

print("start")

######### As SDK Call #########
lambda_client = boto3.client('lambda',
    endpoint_url='http://localhost.localstack.cloud:4566',
    region_name='us-east-1',
    aws_access_key_id='YOUR_ACCESS_KEY',
    aws_secret_access_key='YOUR_SECRET_KEY',
    aws_session_token='YOUR_SESSION_TOKEN'
)
payload = {
    'name': 'Bob'
}
invoke_response = lambda_client.invoke( # Doesn't need to pass in the endpoint_url apparently?
    FunctionName='hello',
    InvocationType='RequestResponse',
    Payload=json.dumps(payload)
)
js_response = json.loads(
    invoke_response['Payload'].read().decode('utf-8')
)
print(js_response)

######### As REST Call #########
# # url = 'http://localhost.localstack.cloud:4566/2015-03-31/functions/info/invocations' # only for posts
# url = 'http://example.execute-api.localhost.localstack.cloud:4566/dev/info'
# headers = {
#     'X-Amz-Invocation-Type': 'RequestResponse',
#     'X-Amz-Log-Type': 'None',
#     'Content-Type': 'application/json',
#     'Authorization': 'YOUR_SESSION_TOKEN'
# }
# payload = {
#     'name': 'Bob'
# }
# response2 = requests.post(url, headers=headers, data=json.dumps(payload))
# js_response2 = response2.json()
# print(js_response2)