# Example for LocalStack
This is just a variety of lambdas and terraform for them to test out a [LocalStack](https://www.localstack.cloud/) environment.

## LocalStack Notes (for linux)
First off, let's install LocalStack.

* Need to get docker running. If getting an permission denied for `/var/run/docker.sock` with any docker command then need to add docker group to current user. See https://stackoverflow.com/questions/48568172/docker-sock-permission-denied/.
    * Run `sudo usermod -aG docker $USER`
    * Run `newgrp docker`
    * If this doesn't stick around between logins, then may need to do this: https://askubuntu.com/questions/1057258/secondary-user-groups-not-loaded, which basically comments out 2 lines in `/etc/pam.d/lightdm`.
* Install docker compose.
    * For Windows, it comes with DockerDesktop, but that is non-free and not for linux.
    * The docker-compose in apt doesn't seem to work (get chunk errors).
    * Need to download binary from [Docker Compose on GitHub](https://github.com/docker/compose) and follow the instructions. i.e download & rename the binary to `docker-compose`, make it executable (`chmod +x`), and move it to `/usr/local/lib/docker/cli-plugins` creating directories as needed.
* Start Localstack with `docker compose up` (would be `docker-compose up` in Windows with Docker Desktop).
    * Just hit `ctrl-c` to stop it. This will reset all state unless you have set the persistence flag!
* Can check status with `curl -s localhost:4566/_localstack/health | jq` or via [Health](http://localhost:4566/_localstack/health)
* Nothing will be loaded into Localstack at this point though.

### Install this example
Now we'll run this example project's terraform and load everything into LocalStack.
* Probably want to install [OpenTofu](https://opentofu.org/docs/intro/install/deb/) instead of terraform itself.
* In the directory with this file, run `tofu init` and then `tofu apply`.
* Since LocalStack destroys everything each time you restart the container you'll need to rerun the apply each time. You don't need to worry about running a `tofu destroy` explicitly since Tofu is smart enough to realize everything is gone when you run the apply again.

### Run this example
You can test out the loaded infrastructure using some of the supplied scripts.
* Need to have Python 3 installed along with the Boto3 module.
* Run `python3 post.py` to send a message through through SQS and update DynamoDB.
* Run `python3 query.py` to read back the messages from the DynamoDB.
* Run `python3 start-step.py` to run a step function.
* Go to http://console.s3-website.localhost.localstack.cloud:4566/ to check out the static website from a S3 bucket, which has a status on all of the resources created above.

### Troubleshooting
To see logs for lambdas instances:
* Install the [AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html) through its instructions which involve downloading something.
* Optionally, to avoid needing to add the endpoint_url arg each time, install the [aws local cli](https://docs.localstack.cloud/user-guide/integrations/aws-cli/#localstack-aws-cli-awslocal) via `apt install awscli-local`.

Then you can follow along as the logs are updated with `awslocal logs tail /aws/lambda/<lambda name> --follow` or try to get them as laid out below. Alternatively, you can try the instructions below, but they really didn't work for me (they should have).
* See what log groups are available: `awslocal logs describe-log-groups`
* Get the log streams for the lambda's group: `awslocal logs describe-log-streams --log-group-name "/aws/lambda/<lambda name>"`
* Get the contents of the stream: `awslocal logs get-log-events --log-stream-name "<log stream name>" --log-group-name "/aws/lambda/<lambda name>"`, although this doesn't work for me for some reason. Says group doesn't exist, which is weird because the same group just gave a list of the streams.