import boto3
import requests

lambda_client = boto3.client('lambda',
    region_name='us-east-1',
    endpoint_url='http://localhost:4566',
    aws_access_key_id='test',
    aws_secret_access_key='test',
    aws_session_token='test')

def call_lambda():
    response = lambda_client.invoke(
        FunctionName='reader',
        InvocationType='RequestResponse'
    )
    payload = response['Payload'].read().decode('utf-8')
    print(payload)

def call_rest():
    response = requests.get("http://example.execute-api.localhost.localstack.cloud:4566/dev/reader")
    print(response.json())

#call_lambda()
call_rest()