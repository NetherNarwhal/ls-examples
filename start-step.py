import boto3
import json

client = boto3.client(
    'stepfunctions',
    endpoint_url='http://localhost.localstack.cloud:4566',
    region_name='us-east-1',
    aws_access_key_id='YOUR_ACCESS_KEY',
    aws_secret_access_key='YOUR_SECRET_KEY',
    aws_session_token='YOUR_SESSION_TOKEN'
)
try:
    response = client.start_execution(
        stateMachineArn='arn:aws:states:us-east-1:000000000000:stateMachine:NumberProcessorSF',
        input=json.dumps({ 'key': 'value' })
    )
    # Sync execution for express step functions isn't supported by localstack yet.
    # https://docs.localstack.cloud/references/coverage/coverage_stepfunctions/
    # response = client.start_sync_execution(
    #     stateMachineArn='arn:aws:states:us-east-1:000000000000:stateMachine:NumberProcessorSF',
    #     input=json.dumps({ 'key': 'value' })
    # )

    print(response)
except Exception as error:
    print(error)