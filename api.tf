resource "aws_api_gateway_rest_api" "api" {
    name        = "ExampleAPI"
    description = "API for Example Lambda Function"

    body = jsonencode({
        openapi = "3.0.0"
        paths = {
            # "/{proxy+}" = { # Use "/{proxy+}" when you want to catch everything
            #     x-amazon-apigateway-any-method = {
            #         x-amazon-apigateway-integration = {
            #             httpMethod            = "POST"
            #             type                  = "AWS_PROXY"
            #             uri                   = aws_lambda_function.info2.invoke_arn
            #             # passthroughBehavior   = "WHEN_NO_MATCH"
            #             # contentHandling       = "CONVERT_TO_TEXT"
            #             # timeoutInMillis       = 29000
            #         }
            #     }
            # },
            "/info" = {
                x-amazon-apigateway-any-method = {
                    x-amazon-apigateway-integration = {
                        httpMethod = "POST"
                        type       = "AWS_PROXY"
                        uri        = aws_lambda_function.info.invoke_arn
                    }
                }
            },
            "/info/{proxy+}" = {
                x-amazon-apigateway-any-method = {
                    x-amazon-apigateway-integration = {
                        httpMethod = "POST"
                        type       = "AWS_PROXY"
                        uri        = aws_lambda_function.info.invoke_arn
                    }
                }
            },
            "/sender" = {
                x-amazon-apigateway-any-method = {
                    x-amazon-apigateway-integration = {
                        httpMethod = "POST"
                        type       = "AWS_PROXY"
                        uri        = aws_lambda_function.sender.invoke_arn
                    }
                }
            },
            "/reader" = {
                x-amazon-apigateway-any-method = {
                    x-amazon-apigateway-integration = {
                        httpMethod = "POST"
                        type       = "AWS_PROXY"
                        uri        = aws_lambda_function.reader.invoke_arn
                    }
                }
            },
        }
    })

    tags = {
        "_custom_id_" = "example" # Special localstack tag for a static API ID.
    }
}

resource "aws_api_gateway_deployment" "api" {
    rest_api_id = aws_api_gateway_rest_api.api.id
    stage_name  = "dev"
}

# Don't really need these permissions in localstack by default:
# https://docs.localstack.cloud/user-guide/security-testing/iam-enforcement/
resource "aws_lambda_permission" "api_info_permission" {
    statement_id  = "AllowExecutionFromAPIGateway"
    action        = "lambda:InvokeFunction"
    function_name = aws_lambda_function.info.function_name
    principal     = "apigateway.amazonaws.com"
    source_arn = "${aws_api_gateway_rest_api.api.execution_arn}/*/*"
}

resource "aws_lambda_permission" "api_sender_permission" {
    statement_id  = "AllowExecutionFromAPIGateway"
    action        = "lambda:InvokeFunction"
    function_name = aws_lambda_function.sender.function_name
    principal     = "apigateway.amazonaws.com"
    source_arn = "${aws_api_gateway_rest_api.api.execution_arn}/*/*"
}

resource "aws_lambda_permission" "api_reader_permission" {
    statement_id  = "AllowExecutionFromAPIGateway"
    action        = "lambda:InvokeFunction"
    function_name = aws_lambda_function.reader.function_name
    principal     = "apigateway.amazonaws.com"
    source_arn = "${aws_api_gateway_rest_api.api.execution_arn}/*/*"
}

output "api_gateway_url" {
    #   value = "https://${aws_api_gateway_rest_api.sender_api.id}.execute-api.${var.region}.amazonaws.com/prod"
    value = "http://${aws_api_gateway_rest_api.api.id}.execute-api.localhost.localstack.cloud:4566/dev/<lambda>"
}