variable "localstack_endpoint" {
    type = string
    default = "http://localhost:4566"
}

provider "aws" {
    region = "us-east-1"
    access_key = "test"
    secret_key = "test"
    s3_use_path_style = false
    skip_credentials_validation = true
    skip_metadata_api_check = true
    skip_requesting_account_id = false
    allowed_account_ids = ["000000000000"]

    endpoints {
        # Localstack services: https://docs.localstack.cloud/user-guide/aws/feature-coverage/
        acm             = var.localstack_endpoint
        apigateway      = var.localstack_endpoint # v2 is not free
        cloudwatch      = var.localstack_endpoint
        dynamodb        = var.localstack_endpoint
        events          = var.localstack_endpoint # EventBridge
        iam             = var.localstack_endpoint
        kinesis         = var.localstack_endpoint
        kms             = var.localstack_endpoint
        lambda          = var.localstack_endpoint
        logs            = var.localstack_endpoint
        route53         = var.localstack_endpoint
        # s3              = var.localstack_endpoint
        s3              = "https://s3.localhost.localstack.cloud:4566"
        secretsmanager  = var.localstack_endpoint
        ses             = var.localstack_endpoint # v2 is not free
        sns             = var.localstack_endpoint
        sqs             = var.localstack_endpoint
        stepfunctions   = var.localstack_endpoint
        sts             = var.localstack_endpoint
    }

    default_tags {
      
    }
}