resource "aws_dynamodb_table" "game_table" {
    name = "GameScores"
    billing_mode = "PROVISIONED"
    read_capacity = 20
    write_capacity = 20
    hash_key = "UserId" # partition key
    range_key = "GameTitle" # sort key

    attribute {
        name = "UserId"
        type = "S"
    }
    attribute {
        name = "GameTitle"
        type = "S"
    }
}