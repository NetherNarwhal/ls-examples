variable "bucket_name" {
    description = "Name of the s3 bucket. Must be unique."
    type        = string
    default     = "console"
}

output "website_endpoint" {
    value = "http://console.s3-website.localhost.localstack.cloud:4566/"
}

resource "aws_s3_bucket" "s3_bucket" {
    bucket = var.bucket_name
}

resource "aws_s3_bucket_website_configuration" "s3_bucket" {
    bucket = aws_s3_bucket.s3_bucket.id
    index_document {
        suffix = "index.html"
    }
    error_document {
        key = "error.html"
    }
}

resource "aws_s3_bucket_acl" "s3_bucket" {
    bucket = aws_s3_bucket.s3_bucket.id
    acl    = "public-read"
}

resource "aws_s3_bucket_policy" "s3_bucket" {
    bucket = aws_s3_bucket.s3_bucket.id

    policy = jsonencode({
        Version = "2012-10-17"
        Statement = [{
            Sid       = "PublicReadGetObject"
            Effect    = "Allow"
            Principal = "*"
            Action    = "s3:GetObject"
            Resource = [
                aws_s3_bucket.s3_bucket.arn,
                "${aws_s3_bucket.s3_bucket.arn}/*",
            ]
        }]
    })
}

resource "aws_s3_object" "object_www" {
    depends_on   = [aws_s3_bucket.s3_bucket]
    for_each     = fileset("${path.root}/web/", "*.html")
    bucket       = var.bucket_name
    key          = basename(each.value)
    source       = "${path.root}/web/${each.value}"
    etag         = filemd5("${path.root}/web/${each.value}")
    content_type = "text/html"
    acl          = "public-read"
}

resource "aws_s3_object" "object_assets" {
    depends_on = [aws_s3_bucket.s3_bucket]
    for_each   = fileset("${path.root}/web/", "{*.svg}")
    bucket     = var.bucket_name
    key        = basename(each.value)
    source     = "${path.root}/web/${each.value}"
    etag       = filemd5("${path.root}/web/${each.value}")
    acl        = "public-read"
}

# This bucket serves no real purpose. Just to test the web console.
resource "aws_s3_bucket" "example" {
  bucket = "my-tf-test-bucket"
}