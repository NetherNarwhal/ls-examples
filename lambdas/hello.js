exports.handler = async (event) => {
    const  name = event.name || 'World';
    const response = `Hello, ${name}!`;
    return response;
};