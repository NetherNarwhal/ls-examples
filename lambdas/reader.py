import boto3
import os
import json
from decimal import Decimal

# So we can serialize Decimals into JSON.
def decimal_default(obj):
    if isinstance(obj, Decimal):
        return float(obj)
    raise TypeError

def lambda_handler(event, context):
    # Connect to the DynamoDB table.
    dynamodb = boto3.resource('dynamodb',
        endpoint_url='http://localhost.localstack.cloud:4566')
    table_name = os.getenv('TABLE_NAME')
    table = dynamodb.Table(table_name)

    # Make sure there is some data. This will overwrite the row if it already exists.
    # new_item = {
    #     'UserId': 'John',
    #     'GameTitle': 'Pacman',
    #     'TopScore': 2000
    # }
    # table.put_item(Item=new_item)

    # Read all rows for the table.
    response = table.scan()
    rows = response['Items']
    num_rows = len(rows)

    if num_rows > 0:
        return {
            "statusCode": 200,
            "body": json.dumps({
                "rows": rows,
                "num_rows": num_rows
            }, default=decimal_default)
        }

    else:
        return {
            "statusCode": 500,
            "body": json.dumps({
                "error": "No rows found."
            })
        }