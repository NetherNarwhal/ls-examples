const { Lambda, ListFunctionsCommand } = require("@aws-sdk/client-lambda");
const lambda = new Lambda();
const { CloudWatchLogsClient, DescribeLogStreamsCommand, GetLogEventsCommand } = require("@aws-sdk/client-cloudwatch-logs");
const logs = new CloudWatchLogsClient();
const { S3Client, ListBucketsCommand, GetBucketWebsiteCommand, ListObjectsCommand } = require("@aws-sdk/client-s3");
const s3 = new S3Client();
const { APIGatewayClient, GetRestApisCommand, GetResourcesCommand, GetResourceCommand }
    = require("@aws-sdk/client-api-gateway");
const apiGateway = new APIGatewayClient();
const { SQSClient, ListQueuesCommand, GetQueueAttributesCommand } = require("@aws-sdk/client-sqs");
const sqs = new SQSClient();
const { DynamoDBClient, ListTablesCommand, DescribeTableCommand } = require("@aws-sdk/client-dynamodb");
const dynamo = new DynamoDBClient();
const { SFNClient, ListStateMachinesCommand, ListExecutionsCommand, DescribeExecutionCommand } = require("@aws-sdk/client-sfn");
const stepFunctions = new SFNClient();
const { EventBridgeClient, ListEventBusesCommand, ListRulesCommand } = require("@aws-sdk/client-eventbridge");
const eventBridge = new EventBridgeClient();
const { SNSClient, ListTopicsCommand } = require("@aws-sdk/client-sns");
const sns = new SNSClient();
const { IAMClient, ListUsersCommand, ListRolesCommand } = require("@aws-sdk/client-iam");
const iam = new IAMClient();


const headers = {
    "Content-Type": "application/json",
    "Access-Control-Allow-Origin": "*",
    "Access-Control-Allow-Methods": "*",
    "Access-Control-Allow-Headers": "Content-Type",
};

function sendError(message, code=500) {
    return { statusCode: code, headers, body: JSON.stringify({message}) };
}

function sendSuccess(payload={}, code=200) { // payload should be json serializable.
    return { statusCode: code, headers, body: JSON.stringify(payload) };
}

async function getLambdas() {
    try {
        const data = await lambda.send(new ListFunctionsCommand({ MaxItems: 50 }));
        const lambdas = [];
        if (!data.Functions) return response;
        for (const lambda of data.Functions) {
            lambdas.push({
                name: lambda.FunctionName,
                runtime: lambda.Runtime,
                isHotLoaded: lambda.CodeSha256.startsWith("hot-reloading"),
            });
        }
        return lambdas;

    } catch (error) {
        console.log(error);
        return { error: `${error.name}: ${error.message}` };
    }
}

async function getLogsForLambda(lambdaName, lines=20) {
    const params = {
        logGroupName: `/aws/lambda/${lambdaName}`,
        orderBy: "LastEventTime",
    };

    try {
        const logStreams = await logs.send(new DescribeLogStreamsCommand(params));
        // Get the most current log stream.
        const current = logStreams.logStreams.sort((a,b) => b.creationTime - a.creationTime)[0];
        console.log("match:", (current === logStreams.logStreams[0]))

        const logStreamName = logStreams.logStreams[0].logStreamName;
        const logEventsParams = {
            logGroupName: params.logGroupName,
            logStreamName: logStreamName,
            limit: lines,
            startFromHead: false,
        };

        const logEvents = await logs.send(new GetLogEventsCommand(logEventsParams));
        return logEvents.events.map(event => event.message); // Also has timestamp and ingestionTime fields.
    } catch (error) {
        console.log(error);
        return { error: `${error.name}: ${error.message}` };
    }
}

async function getBuckets() {
    try {
        const data = await s3.send(new ListBucketsCommand({}));

        // See if they have websites associated with them.
        response = [];
        if (!data.Buckets) return response;
        for (const bucket of data.Buckets) {
            try {
                const webData = await s3.send(new GetBucketWebsiteCommand({ Bucket: bucket.Name }));
                response.push({
                    name: bucket.Name,
                    isWebsite: true,
                    index: webData?.IndexDocument?.Suffix,
                    errorPage: webData?.ErrorDocument?.Key
                });
            } catch (error) {
                if (error.name === "NoSuchWebsiteConfiguration")
                    response.push({ name: bucket.Name, isWebsite: false});
                else throw error;
            }
        }
        return response;
    } catch (error) {
        console.log(error);
        return { error: `${error.name}: ${error.message}` };
    }
}

async function getBucketObjects(bucketName) {
    try {
        const data = await s3.send(new ListObjectsCommand({ Bucket: bucketName }));
        console.log(data.Contents);
        if (data?.Contents === undefined || data.Contents.length === 0) return [];
        const objectKeys = data.Contents.map(obj => obj.Key);
        return objectKeys;
    } catch (error) {
        console.log(error);
        return { error: `${error.name}: ${error.message}` };
    }
}

async function getAPIs() {
    try {
        const apis = await apiGateway.send(new GetRestApisCommand({}));
        const apiNames = apis.items.map(item => {return { name: item.name, id: item.id }});
        return apiNames;
    } catch (error) {
        console.log(error);
        return { error: `${error.name}: ${error.message}` };
    } 
}

async function getApiPathsAndMethods(apiId) {
    try {
        const data = await apiGateway.send(new GetResourcesCommand({ restApiId: apiId }));
        const response = [];
        if (!data.items) return response;
        for (const resource of data.items) {
            const detailedResource = await apiGateway.send(
                new GetResourceCommand({ restApiId: apiId, resourceId: resource.id }));

            if (!detailedResource.resourceMethods) continue;
            for (const methodName in detailedResource.resourceMethods) {
                const method = detailedResource.resourceMethods[methodName];
                const integration = method.methodIntegration;
                if (!integration || !integration.uri) {
                    response.push({ path: detailedResource.path, method: methodName, integration: "NONE" });
                } else {
                    // "...:000000000000:function:info2/invocations"
                    const start = integration.uri.lastIndexOf(":");
                    const lambda = integration.uri.substring(start + 1, integration.uri.length - 12);
                    response.push({ path: detailedResource.path, method: methodName, integration: lambda });
                }
            }
        }
        return response;
    } catch (error) {
        console.log(error);
        return { error: `${error.name}: ${error.message}` };
    }
}

async function getQueues() {
    try {
        const data = await sqs.send(new ListQueuesCommand({}));
        const response = [];
        if (!data.QueueUrls) return response;
        for (const queue of data.QueueUrls) {
            const numMessages = await getNumberOfMessages(queue);
            response.push({ name: queue.substring(queue.lastIndexOf("/") + 1), numMessages });
        }
        return response;
    } catch (error) {
        console.log(error);
        return { error: `${error.name}: ${error.message}` };
    }
}

async function getNumberOfMessages(queueUrl) {
    try {
        const data = await sqs.send(new GetQueueAttributesCommand({
            QueueUrl: queueUrl,
            AttributeNames: ["ApproximateNumberOfMessages"]
        }));
        return parseInt(data.Attributes.ApproximateNumberOfMessages);
    } catch (error) {
        console.log(error);
        throw error;
    }
}

async function getTables() {
    try {
        const data = await dynamo.send(new ListTablesCommand({}));
        const response = [];
        if (!data.TableNames) return response;
        for (const tableName of data.TableNames) {
            const tableInfo = await getTableInfo(tableName);
            response.push({ name: tableName, numItems: tableInfo.ItemCount });
        }
        return response;

    } catch (error) {
        console.log(error);
        return { error: `${error.name}: ${error.message}` };
    }
}

async function getTableInfo(tableName) {
    try {
        const data = await dynamo.send(new DescribeTableCommand({ TableName: tableName }));
        return data.Table;
    } catch (error) {
        console.log(error);
        throw error;
    }
}

async function getStateMachines() {
    try {
        const data = await stepFunctions.send(new ListStateMachinesCommand({}));
        const response = [];
        if (!data.stateMachines) return response;
        for (const stateMachine of data.stateMachines) {
            const executionsData = await stepFunctions.send(
                new ListExecutionsCommand({ stateMachineArn: stateMachine.stateMachineArn }));

            const results = {};
            for (const execution of executionsData.executions) {
                results[execution.status] = (results[execution.status] || 0) + 1;
            }

            response.push({
                name: stateMachine.name,
                type: stateMachine.type, // STANDARD or EXPRESS
                arn: stateMachine.stateMachineArn,
                executions: results,
            });
        }
        return response;
    } catch (error) {
        console.log(error);
        return { error: `${error.name}: ${error.message}` };
    }
}

async function getStateExecutionInfo(stateMachineArn, executionArn) {
    try {
        const data = await stepFunctions.send(
            new DescribeExecutionCommand({ stateMachineArn, executionArn })
        );
        return data;
    } catch (error) {
        console.log(error);
        throw error;
    }
}

async function getEventBuses() {
    try {
        const data = await eventBridge.send(new ListEventBusesCommand({}));
        const response = [];
        if (!data.EventBuses) return response;
        for (const bus of data.EventBuses) {
            response.push({
                name: bus.Name,
                arn: bus.Arn,
            });
        }
        return response;
    } catch (error) {
        console.error(error);
        return { error: `${error.name}: ${error.message}` };
    }
}

async function getEventBridgeRules(busName="default") {
    try {
        const data = await eventBridge.send(new ListRulesCommand({ EventBusName: busName }));
        const response = [];
        if (!data.Rules) return response;
        for (const rule of data.Rules) {
            response.push({
                name: rule.Name,
                arn: rule.Arn,
                state: rule.State,
                description: rule.Description,
            });
        }
        return response;
    } catch (error) {
        console.error(error);
        return { error: `${error.name}: ${error.message}` };
    }
}

async function getSnsTopics() {
    try {
        const data = await sns.send(new ListTopicsCommand({}));
        const response = [];
        if (!data.Topics) return response;
        for (const topic of data.Topics) {
            response.push({
                name: topicArn.substring(topicArn.lastIndexOf(":") + 1),
                arn: topic.TopicArn,
            });
        }
        return response;
    } catch (error) {
        console.error(error);
        return { error: `${error.name}: ${error.message}` };
    }
}

async function getUsersAndRoles() {
    try {
        const usersData = await iam.send(new ListUsersCommand({}));
        const rolesData = await iam.send(new ListRolesCommand({}));

        const users = usersData.Users.map(user => ({ name: user.UserName, userId: user.UserId, type: "user" }));
        const roles = rolesData.Roles.map(role => ({ name: role.RoleName, roleId: role.RoleId, type: "role" }));

        return users.concat(roles);
    } catch (error) {
        console.error(error);
        return { error: `${error.name}: ${error.message}` };
    }
}

exports.handler = async (event, context) => {
    console.log("Starting ...");
    if (event.method === "OPTIONS") return sendSuccess({}); // CORS preflight calls.

    if (event.method !== "GET") {
        if (!event.pathParameters.proxy) {
            // IDEA: Return just the services they pass in querystring (all if no querystring)?
            const response = {}; //event};
            response.lambdas = await getLambdas();
            response.buckets = await getBuckets();
            response.apis = await getAPIs();
            response.queues = await getQueues();
            response.tables = await getTables();
            response.states = await getStateMachines();
            response.events = await getEventBuses();
            response.notifications = await getSnsTopics();
            response.identities = await getUsersAndRoles();
            return sendSuccess(response);
        }

        // Parse the pathParameters.proxy (won't contain querystring)
        const parts = event.pathParameters.proxy.split("/");
        const service = parts[0];
        if (service === "lambda") {
            const lambdaName = parts[1];
            const action = parts[2];
            if (action === "logs") {
//                    const lines = parseInt(event.queryStringParameters.lines) || 20;
                return sendSuccess(await getLogsForLambda(lambdaName)); //, lines));
            } else {
                return sendError("Unsupported action: " + action);
            }

        } else if (service === "s3") {
            const bucketName = parts[1];
            const action = parts[2];
            if (action === "files") {
                return sendSuccess(await getBucketObjects(bucketName));
            } else {
                return sendError("Unsupported action: " + action);
            }

        } else if (service === "apis") {
            const apiId = parts[1];
            const action = parts[2];
            if (action === "routes") {
                return sendSuccess(await getApiPathsAndMethods(apiId));
            } else {
                return sendError("Unsupported action: " + action);
            }

        } else {
            return sendError("Unsupported service: " + service);
        }

     } else
        return sendError("Unsupported method: " + event.method);
};