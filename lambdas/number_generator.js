exports.handler = async (event) => {
    var number = Math.floor(Math.random() * 100) + 1;
    console.log(`Generated number is: ${number}`);
    // return { number: number, is_even: number % 2 === 0 };

    return {
        statusCode: 200,
        headers: { "Content-Type": "application/json", },
        body: JSON.stringify({ number: number, is_even: number % 2 === 0 })
    };
}