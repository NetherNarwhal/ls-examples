exports.handler = async event => {
    console.log(`My odd number is: ${event.number}`);
    // return { number: event.number };

    return {
        statusCode: 200,
        headers: { "Content-Type": "application/json", },
        body: JSON.stringify({ number: event.number })
    };
};