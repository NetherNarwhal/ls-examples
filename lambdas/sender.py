import boto3
import os
import json

def lambda_handler(event, context):
    sqs = boto3.client("sqs")
    queue_url = os.getenv("SQS_QUEUE_URL")

    response = sqs.send_message(
            QueueUrl=queue_url,
            MessageBody=json.dumps({
                "user": "Bob",
                "text": "Hello from the sender lambda!"
            })
        )

    # return response
    return {
        "isBase64Encoded": False,
        "statusCode": 200,
        "headers": { "Content-Type": "application/json" },
        "body": json.dumps(response)
    }