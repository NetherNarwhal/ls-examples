import boto3
import json
import os

def lambda_handler(event, context):
    records = event['Records']

    # Connect to the DynamoDB table.
    dynamodb = boto3.resource(
        'dynamodb',
        endpoint_url='http://localhost.localstack.cloud:4566' # Not 'http://localhost:4566' like it was on windows?
    )
    table_name = os.getenv('TABLE_NAME')
    table = dynamodb.Table(table_name)

    # There can be many messages (records) in a single call as it batches them up.
    for record in records:
        message_body = record['body']
        print(f'Received message: {message_body}')

        # If the message body is a JSON string, you can convert it to a dictionary with json.loads.
        try:
            message_dict = json.loads(message_body)
            print(f'Message as dict: {message_dict}')

            new_item = {
                'UserId': message_dict['user'],
                'GameTitle': message_dict['text'],
                'TopScore': 2000
            }
            table.put_item(Item=new_item)

        except json.JSONDecodeError:
            err_msg = 'Message body is not a valid JSON string'
            print(err_msg)
            raise ValueError(err_msg)

        except Exception as e:
            print(f'Unknown exception: {e}')
            raise e

    return {
        'statusCode': 200,
        'body': json.dumps({
            'message': f'Successfully processed {len(records)} messages'
        })
    }