exports.handler = async event => {
    console.log(`My even number is: ${event.number}`);
    // return { number: event.number };

    return {
        statusCode: 200,
        headers: { "Content-Type": "application/json", },
        body: JSON.stringify({ number: event.number })
    };
};