# import boto3
# lambda_client = boto3.client('lambda',
#     region_name='us-east-1',
#     endpoint_url='http://localhost:4566',
#     aws_access_key_id='test',
#     aws_secret_access_key='test',
#     aws_session_token='test')

# response = lambda_client.invoke(
#     FunctionName='sender',
#     InvocationType='RequestResponse'
# )
# payload = response['Payload'].read().decode('utf-8')
# print(payload)

# Calling via APIGateway REST call
import requests
# URL in local stack is different.
# AWS is "https://{api_gatewayapi.id}.execute-api.<region>.amazonaws.com/<stage>/<lambda>"
response = requests.get("http://example.execute-api.localhost.localstack.cloud:4566/dev/sender")
print(response.json())