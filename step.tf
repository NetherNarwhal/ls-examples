resource "aws_iam_role" "step_functions_role" {
    name = "step_functions_role"
    assume_role_policy = jsonencode({
        Version = "2012-10-17"
        Statement = [{
                Action = "sts:AssumeRole"
                Effect = "Allow"
                Principal = {
                    Service = "states.amazonaws.com"
                }
        }]
    })
}

data "aws_iam_policy_document" "lambda_access_policy" {
    statement {
        actions = [ "lambda:*" ]
        resources = [ "*" ]
    }
}

resource "aws_iam_policy" "step_functions_policy_lambda" {
    name   = "step_functions_policy_lambda_policy_all_poc"
    policy = data.aws_iam_policy_document.lambda_access_policy.json
}

resource "aws_iam_role_policy_attachment" "step_functions_to_lambda" {
    role       = aws_iam_role.step_functions_role.name
    policy_arn = aws_iam_policy.step_functions_policy_lambda.arn
}

resource "aws_sfn_state_machine" "number_processor" {
    name     = "NumberProcessorSF"
    role_arn = aws_iam_role.step_functions_role.arn
#    type     = "EXPRESS"

    definition = <<EOF
{
    "Comment": "execute lambdas",
    "StartAt": "NumberGenerator",
    "States": {
        "NumberGenerator": {
            "Type": "Task",
            "Resource": "${aws_lambda_function.number_generator.arn}",
            "Next": "IsNumberEven"
        },
        "IsNumberEven": {
            "Type": "Choice",
            "Choices": [
            {
                "Variable": "$.Payload.is_even",
                "BooleanEquals": true,
                "Next": "Even"
            }
            ],
            "Default": "Odd"
        },
        "Even": {
            "Type": "Task",
            "Resource": "${aws_lambda_function.even.arn}",
            "End": true
        },
        "Odd": {
            "Type": "Task",
            "Resource": "${aws_lambda_function.odd.arn}",
            "End": true
        }
    }
}
EOF
}

output "step_function_arn" {
    value = aws_sfn_state_machine.number_processor.arn
}