locals {
    # Must be full path. Could also be abspath(path.module) or abspath(path.root)
    lambda_path = "${path.cwd}/lambdas"
}
# Assumes we are using [hot reloading](https://docs.localstack.cloud/user-guide/lambda-tools/hot-reloading/)

data "aws_iam_policy_document" "lambda_assume_role" {
    statement {
        effect = "Allow"
        principals {
            type        = "Service"
            identifiers = ["lambda.amazonaws.com"]
        }
        actions = ["sts:AssumeRole"]
    }
}

resource "aws_iam_role" "lambda_role" {
    name               = "lambda_role"
    assume_role_policy = data.aws_iam_policy_document.lambda_assume_role.json
}

# resource "aws_iam_role" "lambda_role" {
#     name = "lambda_role"
#     assume_role_policy = <<EOF
# {
#     "Version": "2012-10-17",
#     "Statement": [
#         {
#             "Action": "sts:AssumeRole",
#             "Principal": {
#                 "Service": "lambda.amazonaws.com"
#             },
#             "Effect": "Allow",
#             "Sid": ""
#         }
#     ]
# }
# EOF
# }

# resource "aws_iam_role_policy" "lambda_role_db_policy" {
#     name = "lambda_role_db_policy"
#     role = aws_iam_role.lambda_role.id

#     policy = <<EOF
# {
#     "Version": "2012-10-17",
#     "Statement": [
#         {
#             "Action": [
#                 "dynamodb:PutItem",
#                 "dynamodb:Scan"
#             ],
#             "Resource": "${aws_dynamodb_table.game_table.arn}",
#             "Effect": "Allow"
#         }
#     ]
# }
# EOF
# }

################ FOR INTERACTING WITH SQS & DYNAMODB ################
resource "aws_lambda_function" "sender" {
    s3_bucket        = "hot-reload"
    s3_key           = local.lambda_path
    source_code_hash = filebase64sha256("${local.lambda_path}/sender.py")

    function_name    = "sender"
    description      = "Sends messages to SQS"
    role             = aws_iam_role.lambda_role.arn
    handler          = "sender.lambda_handler"
    runtime          = "python3.12"

    environment {
        variables = {
            SQS_QUEUE_URL = aws_sqs_queue.sqs_queue.url
        }
    }
}

resource "aws_lambda_function" "receiver" {
    s3_bucket        = "hot-reload"
    s3_key           = local.lambda_path
    source_code_hash = filebase64sha256("${local.lambda_path}/receiver.py")

    function_name    = "receiver"
    description      = "Receives messages from SQS and saves them to DynamoDB"
    role             = aws_iam_role.lambda_role.arn
    handler          = "receiver.lambda_handler"
    runtime          = "python3.12"

    environment {
        variables = {
            TABLE_NAME = aws_dynamodb_table.game_table.name
        }
    }
}

resource "aws_lambda_event_source_mapping" "sqs_event_source" {
    event_source_arn = aws_sqs_queue.sqs_queue.arn
    function_name    = aws_lambda_function.receiver.function_name
}

resource "aws_lambda_function" "reader" {
    s3_bucket        = "hot-reload"
    s3_key           = local.lambda_path
    source_code_hash = filebase64sha256("${local.lambda_path}/reader.py")

    function_name    = "reader"
    description      = "Queries a DynamoDB table and returns the results"
    role             = aws_iam_role.lambda_role.arn
    handler          = "reader.lambda_handler"
    runtime          = "python3.12"

    environment {
        variables = {
            TABLE_NAME = aws_dynamodb_table.game_table.name
        }
    }
}

################ FOR CONSOLE INFO ################
resource "aws_lambda_function" "info" {
    s3_bucket        = "hot-reload"
    s3_key           = local.lambda_path
    source_code_hash = filebase64sha256("${local.lambda_path}/info.js")

    function_name    = "info"
    description      = "Returns information about the aws environment"
    role             = aws_iam_role.lambda_role.arn
    handler          = "info.handler"
    runtime          = "nodejs20.x"
}

################ FOR TESTING ################
resource "aws_lambda_function" "hello" {
    s3_bucket        = "hot-reload"
    s3_key           = local.lambda_path
    source_code_hash = filebase64sha256("${local.lambda_path}/hello.js")

    function_name    = "hello"
    description      = "Takes in a name and sends a response saying hello to it"
    role             = aws_iam_role.lambda_role.arn
    handler          = "hello.handler"
    runtime          = "nodejs20.x"
}

################ FOR STEP FUNCTIONS ################
resource "aws_lambda_function" "number_generator" {
    s3_bucket        = "hot-reload"
    s3_key           = local.lambda_path
    source_code_hash = filebase64sha256("${local.lambda_path}/number_generator.js")

    function_name    = "number_generator"
    description      = "Generates a random number between 1 and 100"
    role             = aws_iam_role.lambda_role.arn
    handler          = "number_generator.handler"
    runtime          = "nodejs20.x"
}

resource "aws_lambda_function" "even" {
    s3_bucket        = "hot-reload"
    s3_key           = local.lambda_path
    source_code_hash = filebase64sha256("${local.lambda_path}/even.js")

    function_name    = "even"
    description      = "Handles even numbers"
    role             = aws_iam_role.lambda_role.arn
    handler          = "even.handler"
    runtime          = "nodejs20.x"
}

resource "aws_lambda_function" "odd" {
    s3_bucket        = "hot-reload"
    s3_key           = local.lambda_path
    source_code_hash = filebase64sha256("${local.lambda_path}/odd.js")

    function_name    = "odd"
    description      = "Handles odd numbers"
    role             = aws_iam_role.lambda_role.arn
    handler          = "odd.handler"
    runtime          = "nodejs20.x"
}